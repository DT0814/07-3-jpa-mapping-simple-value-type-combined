package com.twuc.webApp.domain.composite;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tao.dong
 */
public interface UserProfileRepository extends JpaRepository<UserProfile,Long> {
}
