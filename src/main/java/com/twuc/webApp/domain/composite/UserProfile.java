package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
@Table(name = "userProfile")
public class UserProfile {
    @Id
    @GeneratedValue
    private Long id;
    @Column(length = 128,nullable = false)
    private String addressCity;
    @Column(length = 128,nullable = false)
    private String addressStreet;

    public UserProfile(String addressCity, String addressStreet) {
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
    }

    public UserProfile() {
    }

    public Long getId() {
        return id;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }
}

