package com.twuc.webApp.domain;

import com.twuc.webApp.domain.composite.CompanyProfile;
import com.twuc.webApp.domain.composite.CompanyProfileRepository;
import com.twuc.webApp.domain.composite.UserProfile;
import com.twuc.webApp.domain.composite.UserProfileRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SimpleMappingAndValueTypeTest extends JpaTestBase {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Test
    void test_save_user_profile() {
        //Arrange
        UserProfile tianguba = new UserProfile("xi'an", "tianguba");
        //Act
        userProfileRepository.saveAndFlush(tianguba);
        Optional<UserProfile> res = userProfileRepository.findById(tianguba.getId());
        UserProfile userProfile = res.get();
        //Assert
        assertNotNull(userProfile);
        Assertions.assertEquals("xi'an",userProfile.getAddressCity());
        Assertions.assertEquals("tianguba",userProfile.getAddressStreet());
    }

    @Test
    void test_save_company_profile() {
        //Arrange

        //Act
        CompanyProfile tiangujiu = new CompanyProfile("xi'an", "tiangujiu");
        companyProfileRepository.saveAndFlush(tiangujiu);
        Optional<CompanyProfile> res = companyProfileRepository.findById(tiangujiu.getId());
        CompanyProfile companyProfile = res.get();
        //Assert
        assertNotNull(companyProfile);
        Assertions.assertEquals("xi'an",companyProfile.getCity());
        Assertions.assertEquals("tiangujiu",companyProfile.getStreet());
    }
}
